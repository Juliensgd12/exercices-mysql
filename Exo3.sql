#FIRSTPART

#Exo3.1

SELECT * 
FROM lpecom_livres;

#Exo3.2

SELECT *
FROM lpecom_livres
WHERE prix > 20;

#Exo3.3

SELECT *
FROM lpecom_livres
ORDER BY prix DESC;

#Exo3.4

SELECT MAX(prix)
FROM lpecom_livres;

#Exo3.5

SELECT *
FROM lpecom_livres
WHERE prix BETWEEN 20 AND 22;

#Exo3.6

SELECT *
FROM lpecom_livres
WHERE isbn_10 != 2092589547;

#Exo3.7

SELECT MIN(prix) AS minus
FROM lpecom_livres;

#Exo3.8

SELECT *
FROM lpecom_livres
LIMIT 3 OFFSET 1;

#SECONDPART

#Exo3.1

SELECT DISTINCT id_etudiant
FROM lpecom_examens;

#Exo 3.2

SELECT COUNT(DISTINCT id_etudiant)
FROM lpecom_examens;

#Exo3.3

SELECT AVG(note)
FROM lpecom_examens
WHERE id_examen = 45;

#Exo3.4

SELECT MAX(note)
FROM lpecom_examens
WHERE id_examen = 87;

#Exo3.5

SELECT DISTINCT id_etudiant
FROM lpecom_examens
WHERE (id_examen = 45 AND note > 11)
OR (id_examen = 87 AND note > 12);

#Exo3.6

SELECT exam.*, etd.prenom, etd.nom
FROM lpecom_examens AS exam
LEFT JOIN lpecom_etudiants AS etd ON exam.id_etudiant = etd.id_etudiant;

#Exo3.7

SELECT exam.*, etd.prenom, etd.nom
FROM lpecom_examens AS exam
INNER JOIN lpecom_etudiants AS etd ON exam.id_etudiant = etd.id_etudiant;

#Exo3.8

SELECT etd.prenom, etd.nom, AVG(exam.note) as Moyenne
FROM lpecom_examens AS exam
INNER JOIN lpecom_etudiants AS etd ON exam.id_etudiant = etd.id_etudiant
WHERE etd.id_etudiant = 30;

#Exo3.9

SELECT *
FROM lpecom_examens AS exam
INNER JOIN lpecom_etudiants AS etd ON exam.id_etudiant = etd.id_etudiant
ORDER BY note DESC
LIMIT 3; 

#3rdPART

#Exo3.1

SELECT id, prenom, nom
FROM lpecom_realisateurs
WHERE nation = "us"
AND sexe = 1;

#Exo3.2

SELECT *
FROM lpecom_realisateurs
WHERE sexe = "0"
ORDER BY nom DESC
LIMIT 1;

#Exo3.3

SELECT f.id, f.nom AS film, r.prenom, r.nom
FROM lpecom_films AS f
INNER JOIN lpecom_realisateurs AS r ON f.id_realisateur = r.id
ORDER BY f.id ASC;

#Exo3.4

SELECT f.id, f.nom AS film, r.prenom, r.nom
FROM lpecom_films AS f
LEFT JOIN lpecom_realisateurs AS r ON f.id_realisateur = r.id
ORDER BY f.id ASC;

#Exo3.5

SELECT f.id, f.nom, fn.note
FROM lpecom_films AS f
LEFT JOIN lpecom_films_notes AS fn ON f.id = fn.id_film
ORDER BY f.id ASC;

#Exo3.6

SELECT f.nom, r.prenom AS realisateur_prenom, r.nom AS realisateur_nom, AVG(fn.note) AS moyenne_note
FROM lpecom_films AS f
INNER JOIN lpecom_realisateurs AS r ON f.id_realisateur = r.id
INNER JOIN lpecom_films_notes AS fn ON f.id = fn.id_film
WHERE f.id = 546;

#Exo3.7

SELECT r.nation, AVG(fn.note) AS moyenne_note
FROM lpecom_films AS f
INNER JOIN lpecom_realisateurs AS r ON f.id_realisateur = r.id
INNER JOIN lpecom_films_notes AS fn ON f.id = fn.id_film
WHERE r.nation = "us";

#Exo3.8 

SELECT r.nation, MAX(fn.note) AS max_note
FROM lpecom_films AS f
INNER JOIN lpecom_realisateurs AS r ON f.id_realisateur = r.id
INNER JOIN lpecom_films_notes AS fn ON f.id = fn.id_film
WHERE r.nation = "uk";

#4thPART

#Exo3.1

SELECT *
FROM lpecom_cities
WHERE gps_lat = 48.66913724637683 AND gps_lng = 1.87586057971015;

#Exo3.2

SELECT COUNT(*)
FROM lpecom_cities
WHERE department_code = 91;

#Exo3.3

SELECT COUNT(*)
FROM lpecom_cities
WHERE name LIKE "%-le-Roi";

#Exo3.4

SELECT COUNT(zip_code) AS n_cities
FROM lpecom_cities
WHERE zip_code = 77320;

#Exo3.5

SELECT COUNT(*)
FROM lpecom_cities
WHERE department_code = 77 AND name LIKE "Saint-%";

#Exo3.6

SELECT *
FROM lpecom_cities
WHERE zip_code BETWEEN 77210 AND 77810;

#Exo3.7

SELECT *
FROM lpecom_cities
WHERE department_code = 77
ORDER BY zip_code DESC
LIMIT 2;

#Exo3.8

SELECT MAX(zip_code)
FROM lpecom_cities;

#Exo3.9

SELECT dep.name AS departement, reg.name AS region, dep.slug
FROM lpecom_departments AS dep
INNER JOIN lpecom_regions AS reg ON dep.region_code = reg.code
WHERE dep.region_code IN (75, 27, 53, 84, 93);

#Exo3.10

SELECT r.name AS reg, d.name AS dep, city.name AS ville
FROM lpecom_cities AS city
INNER JOIN lpecom_departments AS d ON (city.department_code = d.code)
INNER JOIN lpecom_regions AS r ON (d.region_code = r.code)
WHERE d.code = 77;

#5thPART

#Exo3.1

SELECT *
FROM lpecom_covid 
WHERE jour = '2021-04-01';

#Exo3.2

SELECT reg.name, covid.*
FROM lpecom_covid AS covid
INNER JOIN lpecom_regions AS reg ON covid.id_region = reg.code
WHERE jour = '2021-04-01';

#Exo3.3

SELECT SUM(n_dose1)
FROM lpecom_covid
WHERE jour <= '2020-12-31';

SELECT SUM(n_dose2)
FROM lpecom_covid c
WHERE jour <= '2020-12-31';

#Exo3.4

SELECT SUM(n_dose1)
FROM lpecom_covid
WHERE id_region = '93' AND jour 
BETWEEN '2021-03-01' AND '2021-03-31';

#Exo3.5

SELECT SUM(n_dose2)
FROM lpecom_covid 
WHERE id_region = '11' AND jour 
BETWEEN '2021-03-01' AND '2021-03-31';

#Exo3.6

SELECT MAX(n_dose1)
FROM lpecom_covid;

SELECT cvd.*, reg.name
FROM lpecom_covid AS cvd
INNER JOIN lpecom_regions AS reg ON cvd.id_region = reg.code
WHERE cvd.n_dose1 >= 56661;

#Exo3.7

SELECT MAX(n_dose2)
FROM lpecom_covid;

SELECT cvd.*, reg.name
FROM lpecom_covid AS cvd
INNER JOIN lpecom_regions AS reg ON cvd.id_region = reg.code
WHERE cvd.n_dose2 >= 21524;

#Exo3.8

SELECT MAX(couv_dose1)
FROM lpecom_covid;

SELECT cvd.*, reg.name
FROM lpecom_covid AS cvd
INNER JOIN lpecom_regions AS reg ON cvd.id_region = reg.code
WHERE cvd.couv_dose1 >= 19.7;

SELECT MAX(couv_dose2)
FROM lpecom_covid;

SELECT cvd.*, reg.name
FROM lpecom_covid AS cvd
INNER JOIN lpecom_regions AS reg ON cvd.id_region = reg.code
WHERE cvd.couv_dose2 >= 8;

#Exo3.9

SELECT MIN(lpecom_covid.couv_dose1)
FROM lpecom_covid 
WHERE lpecom_covid.jour = '2021-04-06';

SELECT cvd.*, reg.name
FROM lpecom_covid AS cvd
INNER JOIN lpecom_regions AS reg ON cvd.id_region = reg.code
WHERE cvd.jour = '2021-04-06'
AND cvd.couv_dose1 <= 2.80;

#Exo3.10

SELECT AVG(cvd.couv_dose1) AS couv_dose1_avg, AVG(cvd.couv_dose2) AS couv_dose2_avg
FROM lpecom_covid AS cvd
WHERE cvd.jour = '2021-04-06';

#Exo3.11

SELECT lpecom_covid.*, reg.name
FROM lpecom_covid
INNER JOIN lpecom_regions AS reg ON lpecom_covid.id_region = reg.code
WHERE lpecom_covid.couv_dose1 >= 15
AND lpecom_covid.couv_dose2 >= 5
AND lpecom_covid.jour = '2021-04-06';