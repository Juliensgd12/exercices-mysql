#Exo1

SELECT * 
FROM villes_france_free 
ORDER BY ville_population_2012 DESC
LIMIT 10;

#Exo 2

SELECT *
FROM villes_france_free
ORDER BY ville_surface ASC
LIMIT 50;

#Exo 3

SELECT 	ville_departement
FROM villes_france_free
WHERE ville_departement > 97;

#Exo4 

SELECT ville_nom, ville_departement, ville_population_2012
FROM villes_france_free
LEFT JOIN departement 
ON ville_departement = departement.departement_code
ORDER BY ville_population_2012 DESC
LIMIT 10;

#Exo5

SELECT departement_nom, ville_departement, COUNT(*) AS Nb_item
FROM villes_france_free
LEFT JOIN departement 
ON ville_departement = departement.departement_code
GROUP BY ville_departement
ORDER BY Nb_item DESC;

#Exo6

SELECT departement_nom, ville_departement, SUM(ville_surface) AS surface
FROM villes_france_free
LEFT JOIN departement 
ON ville_departement = departement.departement_code
GROUP BY ville_departement
ORDER BY surface ASC;

#Exo7

SELECT COUNT(*) AS Nb_ville
FROM villes_france_free
WHERE ville_nom LIKE 'Saint%';

#Exo8

SELECT  COUNT(*) AS NB_nom_ville, ville_nom
FROM villes_france_free
GROUP BY ville_nom
ORDER BY NB_nom_ville DESC;

#Exo9

SELECT ville_nom, ville_surface
FROM villes_france_free
WHERE ville_surface > ( SELECT AVG(ville_surface)
FROM villes_france_free);

#Exo10

SELECT ville_departement, SUM(ville_population_2012) AS pop_total
FROM villes_france_free
GROUP BY ville_departement
HAVING pop_total > 2000000
ORDER BY pop_total DESC;

#Exo11

UPDATE villes_france_free
SET ville_nom = REPLACE (ville_nom, '-',' ')
WHERE ville_nom LIKE 'Saint-%';


