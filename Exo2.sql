#Exo2.1

SELECT *
FROM client
WHERE prenom = 'Muriel' AND password = SHA1('test11');

#Exo2.2

SELECT COUNT(*) AS produits_cde, nom, quantite
FROM commande_ligne
GROUP BY nom
HAVING produits_cde > 1
ORDER BY produits_cde DESC;

#Exo2.3

SELECT nom, COUNT(*) AS nombres_items, GROUP_CONCAT(commande_id) AS liste_commandes
FROM commande_ligne 
GROUP BY nom 
HAVING nombres_items > 1
ORDER BY nombres_items DESC;

#Exo2.4

UPDATE commande_ligne
SET prix_total = (quantite * prix_unitaire);

#Exo2.5

SELECT prix_total, date_achat, client.nom , client.prenom , commande_id 
FROM client 
LEFT JOIN commande 
ON commande.client_id = client.id 
LEFT JOIN commande_ligne 
ON commande_id = commande.id;

#Exo2.6

UPDATE commande AS tablecommande
INNER JOIN (SELECT commande_id, SUM(commande_ligne.prix_total) AS totalVente
FROM commande_ligne
GROUP BY commande_id ) AS total_commande
ON tablecommande.id =  total_commande.commande_id
SET tablecommande.cache_prix_total = total_commande.totalVente;

#Exo2.7

SELECT MONTH(date_achat) AS mois,
YEAR(date_achat) AS annee, 
SUM(cache_prix_total)
FROM commande
GROUP BY mois, annee
ORDER BY mois, annee;

#Exo2.8

SELECT client.nom, client.prenom, SUM(commande.cache_prix_total) AS total_commande
FROM client
LEFT JOIN commande ON client.id = commande.client_id
GROUP BY client.id
ORDER BY total_commande DESC
LIMIT 10;

#Exo2.9

SELECT date_achat, cache_prix_total
FROM commande
ORDER BY date_achat;

#Exo2.10

ALTER TABLE commande
ADD category INT NOT NULL;

#Exo2.11

UPDATE commande
SET commande.category =
CASE
       WHEN commande.cache_prix_total < 200 THEN 1
       WHEN commande.cache_prix_total < 500 THEN 2
       WHEN commande.cache_prix_total < 1000 THEN 3
       ELSE 4
END;

#Exo2.12

CREATE TABLE commande_category ( id INT PRIMARY KEY AUTO_INCREMENT, description VARCHAR(255) );

#Exo2.13


